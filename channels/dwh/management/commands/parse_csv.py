import importlib

from django.core.management import BaseCommand, CommandError

default_csv = 'sample_data.csv'
dwh_utils = importlib.import_module('dwh.utils')  # explicit import the models module


class Command(BaseCommand):
    help = "csv parser, first arg will be pass like a file_name. the base dir is PROJECT_ROOT/csv_dumps"

    def add_arguments(self, parser):
        parser.add_argument(
            *('--filename', '-F'),
            help='cvs filename to parse',
            nargs='?',
            type=str,
            default=default_csv,
        )

    def handle(self, *args, **options):
        try:
            filename = options['filename']
            manager = dwh_utils.Manager(filename=filename)
            manager.parse_file()
        except Exception as e:
            raise CommandError(repr(e))
