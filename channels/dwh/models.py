from django.db import models


class Stringify:
    """get stringify name """

    name = None

    def __str__(self):
        return f'#{self.name}'


class Channel(models.Model):
    """pivot table with indexes and relations to dictionaries"""

    related_name = 'channels'

    date = models.DateField(db_index=True)
    resource = models.ForeignKey('Resource', on_delete=models.DO_NOTHING, related_name=related_name, db_index=True)
    country = models.ForeignKey('Country', on_delete=models.DO_NOTHING, related_name=related_name, db_index=True)
    operation_system = models.ForeignKey(
        'OperationSystem', on_delete=models.DO_NOTHING, related_name=related_name, db_index=True)
    impressions = models.IntegerField()
    clicks = models.IntegerField()
    installs = models.IntegerField()
    spend = models.FloatField()
    revenue = models.FloatField()

    def __str__(self):
        return f'#{self.resource}. {self.country}, {self.country}, {self.operation_system}'


class Resource(Stringify, models.Model):
    """recourse name"""

    name = models.CharField(max_length=200, db_index=True)


class Country(Stringify, models.Model):
    """country name with code"""

    numeric_value = models.IntegerField(blank=True, default=0)
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=10, db_index=True)


class OperationSystem(Stringify, models.Model):
    """Operation system name"""

    name = models.CharField(max_length=200, db_index=True)
