from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers

from .models import Channel, Resource, Country, OperationSystem


class OperationSystemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationSystem
        fields = ('name',)


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('name', 'code')


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ('name',)


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ('date', 'resource', 'country', 'operation_system',
                  'impressions', 'clicks', 'installs', 'spend', 'revenue',)

    resource = serializers.StringRelatedField()
    country = serializers.StringRelatedField()
    operation_system = serializers.StringRelatedField()


class GroupSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', None)
        super(GroupSerializer, self).__init__(*args, **kwargs)

        # print(fields)

        if fields is not None:
            allowed = set(fields)
            existing = set(self.fields.keys())

            # print(existing - allowed)

            for field_name in existing - allowed:
                self.fields.pop(field_name)

            # print(self.fields)

    class Meta:
        model = Channel
        fields = ('date', 'resource', 'country', 'operation_system',
                  'impressions', 'clicks', 'installs', 'spend', 'revenue',)
