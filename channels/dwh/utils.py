import csv
import os
from datetime import datetime

import pycountry
from django.conf import settings
from django_filters import rest_framework, DateFilter, ModelMultipleChoiceFilter
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination

from .models import Channel, Country, Resource, OperationSystem

CSV_DIR_NAME = 'csv_dumps'
CSV_DUMPS_DIR = os.path.join(settings.BASE_DIR, '..', CSV_DIR_NAME)


class Manager:
    INPUT_DATE_FORMAT = '%d.%m.%Y'
    OUTPUT_DATE_FORMAT = '%Y-%m-%d'

    def __init__(self, filename: str):
        if filename is None or filename == '':
            raise ValueError('filename is not set')

        self.filename = filename

    def parse_file(self) -> bool:
        full_path = os.path.join(CSV_DUMPS_DIR, self.filename)

        if not os.path.isfile(full_path):
            raise FileExistsError(f'file does not exist: {full_path}')

        with open(full_path, 'rt') as target_file:
            reader = csv.reader(target_file)
            for row_num, row in enumerate(reader):

                # skip first or empty row
                if row_num == 0 or len(row) == 0:
                    continue

                date, resource, country_code, operation_system, impressions, clicks, installs, spend, revenue = row
                formatted_date = datetime.strptime(date, Manager.INPUT_DATE_FORMAT)

                channel_obj, created = Channel.objects.get_or_create(
                    date=formatted_date.strftime(Manager.OUTPUT_DATE_FORMAT),

                    resource=Resource.objects.get_or_create(name=resource)[0],
                    country=Manager.get_country(country_code),
                    operation_system=OperationSystem.objects.get_or_create(name=operation_system)[0],

                    impressions=impressions,
                    clicks=clicks,
                    installs=installs,
                    spend=spend,
                    revenue=revenue,
                )
                print(channel_obj)

        return True

    @staticmethod
    def get_country(country_code: str) -> Country:
        country_code = country_code.upper()

        by_alpha_2 = pycountry.countries.get(alpha_2=country_code)
        by_alpha_3 = pycountry.countries.get(alpha_3=country_code)

        country = by_alpha_3 if by_alpha_2 is None else by_alpha_2

        return Country.objects.get_or_create(
            numeric_value=country.numeric,
            name=country.name,
            code=country.alpha_2 or country.alpha_3
        )[0]


class ChannelSetPagination(PageNumberPagination):
    page_size = 10
    max_page_size = 1000
    page_size_query_param = 'page_size'
    last_page_strings = ('last',)


class EventFilter(rest_framework.FilterSet):
    class Meta:
        model = Channel
        fields = ('date_from', 'date_to', 'channel', 'country', 'operation_system')

    date_from = DateFilter(field_name='date', lookup_expr='gte')
    date_to = DateFilter(field_name='date', lookup_expr='lte')

    resources = Resource.objects.all()
    countries = Country.objects.all()
    o_systems = OperationSystem.objects.all()

    channel = ModelMultipleChoiceFilter(field_name='resource', queryset=resources)
    country = ModelMultipleChoiceFilter(field_name='country', queryset=countries)
    operation_system = ModelMultipleChoiceFilter(field_name='operation_system', queryset=o_systems)

