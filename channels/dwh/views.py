from django.db.models import Count, F
from django_filters import rest_framework
from rest_framework import generics, filters
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Channel
from .serializers import ChannelSerializer, GroupSerializer
from .utils import ChannelSetPagination, EventFilter

base_query_params = {'resource': 'name', 'country': 'code', 'operation_system': 'name'}


class ChannelListView(generics.ListAPIView):
    queryset = Channel.objects.all()

    serializer_class = ChannelSerializer
    pagination_class = ChannelSetPagination

    filter_backends = (
        rest_framework.DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter
    )

    filter_class = EventFilter
    ordering_fields = ('resource', 'country', 'operation_system', 'date',
                       'impressions', 'clicks', 'installs', 'spend', 'revenue',)

    search_fields = [f'{param}__{key_word}' for param, key_word in base_query_params.items()]


@api_view(['GET'])
def group_list(request):
    groups = request.query_params.getlist('param', None)
    qs = Channel.objects.all()

    if len(groups) == 0:
        serializer = ChannelSerializer(qs, many=True)
        return Response(serializer.data)

    for group_by in groups:
        if group_by not in base_query_params:
            continue

        suffix = base_query_params[group_by]
        relation = f'{group_by}__{suffix}'
        annotation = {f'{group_by}_count': Count(relation)}
        qs = qs.values(**{relation: F(relation)}).annotate(**annotation)
        print(qs[0].keys())

    serializer = GroupSerializer(qs, fields=qs[0].keys())
    return Response(serializer.data)
